--Alter the rental duration and rental rates of the film you inserted before to three weeks and 9.99, respectively.
UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE film_id = 1004;

/*Alter any existing customer in the database with at least 10 rental and 10 payment records. 
Change their personal data to yours (first name, last name, address, etc.). You can use any existing address from the "address" table. 
Please do not perform any updates on the "address" table, as this can impact multiple records with the same address. */
UPDATE customer
SET
    first_name = 'Muhammadali',
    last_name = 'Ergashaliyev',
    email = 'ali@example.com',
    active = 1
WHERE customer_id IN (
    SELECT c.customer_id
    FROM customer c
    JOIN rental r ON c.customer_id = r.customer_id
    JOIN payment p ON c.customer_id = p.customer_id
    GROUP BY c.customer_id
    HAVING COUNT(DISTINCT r.rental_id) >= 10 AND COUNT(DISTINCT p.payment_id) >= 10
    LIMIT 1 -- Add this line to limit the result to one customer
);

--Add your favorite movies to any store's inventory.
UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id = 1;